CREATE DATABASE test;
\c test
CREATE SCHEMA IF NOT EXISTS tracker;
ALTER DATABASE test SET search_path TO tracker;