package com.kronst.tasktracker.controller;

import com.kronst.tasktracker.data.request.CreateTaskRequest;
import com.kronst.tasktracker.data.request.UpdateTaskRequest;
import com.kronst.tasktracker.data.response.TaskResponse;
import com.kronst.tasktracker.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.net.URI;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author KRonst
 */
@RestController
@RequestMapping("/tasks")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @Operation(description = "Create new task")
    @ApiResponse(responseCode = "201", description = "OK")
    @PostMapping
    public ResponseEntity<?> createTask(@RequestBody CreateTaskRequest request) {
        long id = taskService.save(request);
        URI location = URI.create(String.format("/tasks/%d", id));
        return ResponseEntity.created(location).build();
    }

    @Operation(description = "Update task")
    @ApiResponses({
        @ApiResponse(responseCode = "204", description = "OK"),
        @ApiResponse(responseCode = "404", description = "Task not found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<?> updateTask(
        @PathVariable long id,
        @RequestBody UpdateTaskRequest request
    ) {
        taskService.update(id, request);
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Get info about task")
    @ApiResponses({
        @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = TaskResponse.class))),
        @ApiResponse(responseCode = "404", description = "Task not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<TaskResponse> findById(@PathVariable long id) {
        return ResponseEntity.ok(taskService.findById(id));
    }

    @Operation(description = "Get list of all tasks")
    @ApiResponse(
        responseCode = "200",
        content = @Content(array = @ArraySchema(schema = @Schema(implementation = TaskResponse.class)))
    )
    @GetMapping
    public ResponseEntity<List<TaskResponse>> findAll(
        @RequestParam(required = false) Long departmentId
    ) {
        return ResponseEntity.ok(taskService.findAll(departmentId));
    }
}
