package com.kronst.tasktracker.controller;

import com.kronst.tasktracker.data.response.AttachmentResponse;
import com.kronst.tasktracker.service.AttachmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.net.URI;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author KRonst
 */
@RestController
@RequestMapping("/attachments")
@RequiredArgsConstructor
public class AttachmentController {

    private final AttachmentService attachmentService;

    @Operation(description = "Add attachment to task")
    @ApiResponse(responseCode = "201", description = "OK")
    @PostMapping(consumes = "multipart/form-data")
    public ResponseEntity<?> addAttachment(
        @RequestParam long taskId,
        @RequestParam MultipartFile file
    ) {
        long id = attachmentService.addAttachment(taskId, file);
        URI location = URI.create(String.format("/attachments/%d", id));
        return ResponseEntity.created(location).build();
    }

    @Operation(description = "Get attachment by id")
    @ApiResponses({
        @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = Resource.class))),
        @ApiResponse(responseCode = "404", description = "Attachment not found")
    })
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public HttpEntity<?> findById(@PathVariable long id) {
        AttachmentResponse file = attachmentService.findById(id);
        HttpHeaders headers = getHeaders(file.getName());
        return new HttpEntity<>(file.getContent(), headers);
    }

    private HttpHeaders getHeaders(String fileName) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_OCTET_STREAM_VALUE));
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + fileName);
        return headers;
    }
}
