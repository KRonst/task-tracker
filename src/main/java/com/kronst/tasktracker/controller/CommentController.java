package com.kronst.tasktracker.controller;

import com.kronst.tasktracker.data.request.CreateCommentRequest;
import com.kronst.tasktracker.service.CommentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.net.URI;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author KRonst
 */
@RestController
@RequestMapping("/comments")
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @Operation(description = "Add new comment to a task")
    @ApiResponse(responseCode = "201", description = "OK")
    @PostMapping
    public ResponseEntity<?> addComment(@RequestBody CreateCommentRequest request) {
        long id = commentService.addComment(request);
        URI location = URI.create(String.format("/comments/%d", id));
        return ResponseEntity.created(location).build();
    }

    @Operation(description = "Delete specified comment from task")
    @ApiResponse(responseCode = "204", description = "OK")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable long id) {
        commentService.deleteComment(id);
        return ResponseEntity.noContent().build();
    }
}
