package com.kronst.tasktracker.service;

import com.kronst.tasktracker.data.dto.AttachmentDto;
import com.kronst.tasktracker.data.dto.CommentDto;
import com.kronst.tasktracker.data.dto.DepartmentDto;
import com.kronst.tasktracker.data.dto.UserDto;
import com.kronst.tasktracker.data.request.CreateTaskRequest;
import com.kronst.tasktracker.data.request.UpdateTaskRequest;
import com.kronst.tasktracker.data.response.TaskResponse;
import com.kronst.tasktracker.data.response.UserRateResponse;
import com.kronst.tasktracker.enums.TaskStatus;
import com.kronst.tasktracker.exception.ClientRequestException;
import com.kronst.tasktracker.exception.DepartmentNotFoundException;
import com.kronst.tasktracker.exception.TaskNotFoundException;
import com.kronst.tasktracker.exception.UserNotFoundException;
import com.kronst.tasktracker.external.UserRateClient;
import com.kronst.tasktracker.storage.model.Task;
import com.kronst.tasktracker.storage.model.User;
import com.kronst.tasktracker.storage.repository.AttachmentRepository;
import com.kronst.tasktracker.storage.repository.CommentRepository;
import com.kronst.tasktracker.storage.repository.DepartmentRepository;
import com.kronst.tasktracker.storage.repository.TaskRepository;
import com.kronst.tasktracker.storage.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * @author KRonst
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class TaskService {

    private static final Sort DEFAULT_SORT = Sort.by("createdAt");
    private final TaskRepository taskRepository;
    private final DepartmentRepository departmentRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final AttachmentRepository attachmentRepository;
    private final UserRateClient userRateClient;

    public long save(CreateTaskRequest request) {
        Task task = new Task();
        task.setName(request.getName());
        task.setDescription(request.getDescription());
        task.setStatus(TaskStatus.NEW);
        task.setDepartment(
            departmentRepository.findById(request.getDepartmentId())
                .orElseThrow(() -> new DepartmentNotFoundException(request.getDepartmentId()))
        );
        task.setAuthor(
            userRepository.findById(request.getAuthorId())
                .orElseThrow(() -> new UserNotFoundException(request.getAuthorId()))
        );
        task.setImplementer(
            userRepository.findById(request.getImplementerId())
                .orElseThrow(() -> new UserNotFoundException(request.getImplementerId()))
        );

        return taskRepository.saveAndFlush(task).getId();
    }

    public void update(long id, UpdateTaskRequest request) {
        Task task = findTaskById(id);
        taskRepository.saveAndFlush(updateTaskFields(task, request));
    }

    public TaskResponse findById(long id) {
        Task task = findTaskById(id);
        return convertToResponse(task);
    }

    public List<TaskResponse> findAll(Long departmentId) {
        List<Task> tasks = new ArrayList<>();

        if (departmentId != null) {
            tasks.addAll(taskRepository.findAllByDepartmentId(departmentId, DEFAULT_SORT));
        } else {
            tasks.addAll(taskRepository.findAll(DEFAULT_SORT));
        }

        return tasks.stream()
            .map(this::convertToResponse)
            .collect(Collectors.toList());
    }

    private Task findTaskById(long id) {
        return taskRepository.findById(id)
            .orElseThrow(() -> new TaskNotFoundException(id));
    }

    private Task updateTaskFields(Task task, UpdateTaskRequest request) {
        task.setName(request.getName());
        task.setDescription(request.getDescription());
        task.setStatus(request.getStatus());
        task.setDepartment(
            departmentRepository.findById(request.getDepartmentId()).orElse(null)
        );
        task.setAuthor(
            userRepository.findById(request.getAuthorId()).orElse(null)
        );
        task.setImplementer(
            userRepository.findById(request.getImplementerId()).orElse(null)
        );
        return task;
    }

    private TaskResponse convertToResponse(Task task) {
        User author = task.getAuthor();
        User implementer = task.getImplementer();

        CompletableFuture<UserRateResponse> authorResult = userRateClient.getUserRate(author.getId());
        CompletableFuture<UserRateResponse> implementerResult = userRateClient.getUserRate(implementer.getId());

        TaskResponse response = new TaskResponse();
        response.setId(task.getId());
        response.setName(task.getName());
        response.setDescription(task.getDescription());
        response.setStatus(task.getStatus());
        response.setDepartment(DepartmentDto.from(task.getDepartment()));
        response.setComments(loadComments(task));
        response.setAttachments(loadAttachments(task));
        response.setCreatedAt(task.getCreatedAt());
        response.setUpdatedAt(task.getUpdatedAt());

        int authorRate;
        int implementerRate;
        try {
            authorRate = authorResult.get().getRate();
            implementerRate = implementerResult.get().getRate();
        } catch (ExecutionException | InterruptedException e) {
            log.error("Can't receive user's rate from completable future", e);
            throw new ClientRequestException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.setAuthor(UserDto.from(author, authorRate));
        response.setImplementer(UserDto.from(implementer, implementerRate));

        return response;
    }

    private List<CommentDto> loadComments(Task task) {
        return commentRepository.findAllByTaskId(task.getId())
            .stream()
            .map(CommentDto::from)
            .collect(Collectors.toList());
    }

    private List<AttachmentDto> loadAttachments(Task task) {
        return attachmentRepository.findAllByTaskId(task.getId())
            .stream()
            .map(AttachmentDto::from)
            .collect(Collectors.toList());
    }
}
