package com.kronst.tasktracker.service;

import com.kronst.tasktracker.data.response.AttachmentResponse;
import com.kronst.tasktracker.exception.AttachmentNotFoundException;
import com.kronst.tasktracker.exception.ClientRequestException;
import com.kronst.tasktracker.exception.TaskNotFoundException;
import com.kronst.tasktracker.storage.model.Attachment;
import com.kronst.tasktracker.storage.model.Task;
import com.kronst.tasktracker.storage.repository.AttachmentRepository;
import com.kronst.tasktracker.storage.repository.TaskRepository;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author KRonst
 */
@Service
@RequiredArgsConstructor
public class AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final TaskRepository taskRepository;

    public long addAttachment(long taskId, MultipartFile file) {
        Task task = taskRepository.findById(taskId)
            .orElseThrow(() -> new TaskNotFoundException(taskId));

        try {
            Attachment attachment = new Attachment();
            attachment.setName(file.getOriginalFilename());
            attachment.setContent(file.getBytes());
            attachment.setTask(task);
            return attachmentRepository.saveAndFlush(attachment).getId();
        } catch (IOException ex) {
            throw new ClientRequestException(HttpStatus.INTERNAL_SERVER_ERROR, "Can't save file to database");
        }
    }

    public AttachmentResponse findById(long id) {
        Attachment attachment = attachmentRepository.findById(id)
            .orElseThrow(() -> new AttachmentNotFoundException(id));

        return new AttachmentResponse(attachment.getName(), attachment.getContent());
    }
}
