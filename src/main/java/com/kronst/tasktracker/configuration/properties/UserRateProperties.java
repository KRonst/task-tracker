package com.kronst.tasktracker.configuration.properties;

import java.time.Duration;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * @author KRonst
 */
@ConstructorBinding
@ConfigurationProperties("clients.rest.user-rate")
@Data
public class UserRateProperties {
    private final String baseUrl;
    private final Duration connectionTimeout;
    private final Duration readTimeout;
}
