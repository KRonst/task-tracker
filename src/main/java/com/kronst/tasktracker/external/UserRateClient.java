package com.kronst.tasktracker.external;

import com.kronst.tasktracker.data.response.UserRateResponse;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * @author KRonst
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class UserRateClient {

    private final RestTemplate userRateRestTemplate;

    public CompletableFuture<UserRateResponse> getUserRate(long id) {
        return CompletableFuture.supplyAsync(() -> callService(id));
    }

    private UserRateResponse callService(long id) {
        UserRateResponse response;
        try {
            response = userRateRestTemplate.getForObject("/rates/{id}", UserRateResponse.class, id);
        } catch (RestClientException ex) {
            log.error("Can't get user's rate for id {}", id, ex);
            response = new UserRateResponse(id, -1);
        }

        return response;
    }
}
