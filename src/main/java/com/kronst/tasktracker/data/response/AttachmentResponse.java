package com.kronst.tasktracker.data.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
@AllArgsConstructor
public class AttachmentResponse {
    private String name;
    private byte[] content;
}
