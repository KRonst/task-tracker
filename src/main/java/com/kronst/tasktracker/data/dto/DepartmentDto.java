package com.kronst.tasktracker.data.dto;

import com.kronst.tasktracker.storage.model.Department;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
public class DepartmentDto {
    @Schema(example = "1")
    private long id;
    @Schema(example = "HR Department")
    private String name;
    @Schema(example = "Useful information about HR department")
    private String description;

    public static DepartmentDto from(Department department) {
        DepartmentDto dto = new DepartmentDto();
        dto.setId(department.getId());
        dto.setName(department.getName());
        dto.setDescription(department.getDescription());
        return dto;
    }
}
