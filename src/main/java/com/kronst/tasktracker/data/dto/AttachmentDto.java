package com.kronst.tasktracker.data.dto;

import com.kronst.tasktracker.storage.model.Attachment;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
public class AttachmentDto {
    @Schema(example = "1")
    private long id;
    @Schema(example = "example.jpg")
    private String name;

    public static AttachmentDto from(Attachment attachment) {
        AttachmentDto dto = new AttachmentDto();
        dto.setId(attachment.getId());
        dto.setName(attachment.getName());
        return dto;
    }
}
