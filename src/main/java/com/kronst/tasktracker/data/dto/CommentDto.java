package com.kronst.tasktracker.data.dto;

import com.kronst.tasktracker.storage.model.Comment;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
public class CommentDto {
    @Schema(example = "1")
    private long id;
    @Schema(example = "2")
    private long authorId;
    @Schema(example = "Such WOW!")
    private String text;

    public static CommentDto from(Comment comment) {
        CommentDto dto = new CommentDto();
        dto.setId(comment.getId());
        dto.setAuthorId(comment.getAuthor().getId());
        dto.setText(comment.getText());
        return dto;
    }
}
