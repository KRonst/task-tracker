package com.kronst.tasktracker.data.request;

import com.kronst.tasktracker.enums.TaskStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author KRonst
 */
@Data
public class UpdateTaskRequest {
    @Schema(example = "EXAMPLE-123")
    private String name;
    @Schema(example = "Migrate task tracker service to async stack")
    private String description;
    @Schema(example = "DEVELOPING")
    private TaskStatus status;
    @Schema(example = "1")
    private long departmentId;
    @Schema(example = "2")
    private long authorId;
    @Schema(example = "3")
    private long implementerId;
}
