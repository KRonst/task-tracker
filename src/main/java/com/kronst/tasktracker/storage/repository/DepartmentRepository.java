package com.kronst.tasktracker.storage.repository;

import com.kronst.tasktracker.storage.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KRonst
 */
public interface DepartmentRepository extends JpaRepository<Department, Long> {

}
