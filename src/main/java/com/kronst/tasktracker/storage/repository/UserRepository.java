package com.kronst.tasktracker.storage.repository;

import com.kronst.tasktracker.storage.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author KRonst
 */
public interface UserRepository extends JpaRepository<User, Long> {

}
