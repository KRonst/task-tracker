package com.kronst.tasktracker.exception;

import org.springframework.http.HttpStatus;

/**
 * @author KRonst
 */
public class TaskNotFoundException extends ClientRequestException {

    public TaskNotFoundException(long id) {
        super(HttpStatus.NOT_FOUND, String.format("Task with ID=%d not found", id));
    }
}
