package com.kronst.tasktracker.exception;

import org.springframework.http.HttpStatus;

/**
 * @author KRonst
 */
public class DepartmentNotFoundException extends ClientRequestException {

    public DepartmentNotFoundException(long id) {
        super(HttpStatus.NOT_FOUND, String.format("Department with ID=%d not found", id));
    }
}
