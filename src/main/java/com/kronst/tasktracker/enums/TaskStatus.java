package com.kronst.tasktracker.enums;

/**
 * @author KRonst
 */
public enum TaskStatus {
    NEW,
    ANALYZE,
    READY_TO_DEVELOP,
    DEVELOPING,
    READY_TO_TEST,
    TESTING,
    READY_TO_RELEASE,
    RELEASED
}
