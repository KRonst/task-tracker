DROP TABLE IF EXISTS tracker.attachments;
DROP TABLE IF EXISTS tracker.comments;
DROP TABLE IF EXISTS tracker.tasks;
DROP TABLE IF EXISTS tracker.users;
DROP TABLE IF EXISTS tracker.departments;