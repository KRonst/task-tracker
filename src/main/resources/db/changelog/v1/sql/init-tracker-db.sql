DROP TABLE IF EXISTS tracker.departments;
CREATE TABLE tracker.departments (
    id              BIGSERIAL PRIMARY KEY NOT NULL,
    name            VARCHAR(255) NOT NULL,
    description     TEXT,
    created_at      TIMESTAMP,
    updated_at      TIMESTAMP,
    CONSTRAINT name_unique UNIQUE (name)
);

DROP TABLE IF EXISTS tracker.users;
CREATE TABLE tracker.users (
    id              BIGSERIAL PRIMARY KEY NOT NULL,
    name            VARCHAR(255) NOT NULL,
    department_id   BIGINT REFERENCES departments(id),
    created_at      TIMESTAMP,
    updated_at      TIMESTAMP
);

DROP TABLE IF EXISTS tracker.tasks;
CREATE TABLE tracker.tasks (
    id              BIGSERIAL PRIMARY KEY NOT NULL,
    name            VARCHAR(255) NOT NULL,
    description     TEXT,
    status          VARCHAR(32) NOT NULL,
    department_id   BIGINT REFERENCES departments(id) ON DELETE CASCADE,
    author_id       BIGINT REFERENCES users(id),
    implementer_id  BIGINT REFERENCES users(id),
    created_at      TIMESTAMP,
    updated_at      TIMESTAMP
);

DROP TABLE IF EXISTS tracker.comments;
CREATE TABLE tracker.comments (
    id              BIGSERIAL PRIMARY KEY NOT NULL,
    text            TEXT,
    author_id       BIGINT REFERENCES users(id) ON DELETE CASCADE,
    task_id         BIGINT REFERENCES tasks(id) ON DELETE CASCADE,
    created_at      TIMESTAMP,
    updated_at      TIMESTAMP
);

DROP TABLE IF EXISTS tracker.attachments;
CREATE TABLE tracker.attachments (
    id              BIGSERIAL PRIMARY KEY NOT NULL,
    name            VARCHAR(255) NOT NULL,
    content         BYTEA,
    task_id         BIGINT REFERENCES tasks(id) ON DELETE CASCADE,
    created_at      TIMESTAMP
);
